<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 12.04.2017
  Time: 0:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/registration.css">
    <title>Регистрация</title>
</head>
<body>
<form class="form" method="POST" name="registrForm" action="audio_store">
    <input type="hidden" name="commandObject" value="client">
    <input type="hidden" name="command" value="registration">
    <div class="form-item">
        <label for="name" class="form-item-label">Имя</label>
        <input id="name" class="form-input" name="name">
        <span name="error-name" class="error-message"></span>
    </div>
    <div class="form-item">
        <label for="login" class="form-item-label">Логин</label>
        <input id="login" class="form-input" name="login">
        <span name="error-login" class="error-message"></span>
    </div>
    <div class="form-item">
        <label for="date" class="form-item-label">Дата рождения</label>
        <input type = "date" id="date" class="form-input" name="date">
        <span name="error-date" class="error-message"></span>
    </div>
    <div class="form-item">
        <label for="email" class="form-item-label">E-mail</label>
        <input id="email" class="form-input" name="email">
        <span name="error-email" class="error-message"></span>
    </div>
    <div class="form-item">
        <label for="creditCard" class="form-item-label">Номер кредитной карты</label>
        <input id="creditCard" class="form-input" name="creditCard">
        <span name="error-credit" class="error-message"></span>
    </div>
    <div class="form-item">
        <label for="password" class="form-item-label">Пароль</label>
        <input id="password" class="form-input" type="password" name="password">
        <span name="error-password" class="error-message"></span>
    </div>
    <div class="form-item">
        <label for="passwordRepeat" class="form-item-label">Подтвердите пароль</label>
        <input id="passwordRepeat" class="form-input" type="password" name="passwordRepeat">
        <span name="error-repeat-password" class="error-message"></span>
    </div>
    <div class="form-item">
        <input class ="button-style" type="submit" value="Регистрация">
        <input class ="button-style" type="reset" value="Очистить">
    </div>
    <div class="form-item">
        <input class ="button-style" type="submit" value="Вход">
    </div>
</form>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/MD5.js"></script>
<script src="js/validationClient.js"></script>
<script src="js/registrationClient.js"></script>
</body>
</html>
