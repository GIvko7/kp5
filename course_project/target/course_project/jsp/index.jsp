<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/auth.css">
    <title>Авторизация</title>
</head>
<body>
<div class="wrapper">
    <div id="error" class="error-message">${error}</div>
    <form class="form" method = "POST" action = "audio_store">
        <input type="hidden" name="commandObject" value="client">
        <input type="hidden" name="command" value="login">
        <div class="form-item">
            <label for="login" class="form-item-label">Логин</label>
            <input id="login" class="form-input" name = "login" value="${login}">
        </div>
        <div class="form-item">
            <label for="password" class="form-item-label">Пароль</label>
            <input id="password" class="form-input" type="password" name="password">
        </div>
        <div class="form-item">
            <input type="checkbox" name="role" value=""> Вход в качестве администратора
        </div>
        <div class="form-item">
            <input class = "button-form" type="submit" value="Войти">
            <input class ="button-form" type="submit" value="Регистрация">
            <input class ="button-form" type="reset" value="Очистить">
        </div>
    </form>
</div>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/authorization.js"></script>
</body>
</html>