<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 10.04.2017
  Time: 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/audiotracks.css">
    <title>Аудиотреки</title>
</head>
<body>
<header class="header" method="GET">
    <a href="/course_project/audio_store">Главная</a>
    <a href="/course_project/audio_store?commandObject=audioTrack&command=find">Аудиотреки</a>
    <a href="/course_project/audio_store?commandObject=order&command=find">Корзина</a>
    Рекомендации
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="audioTrack">
    <input type="hidden" name="command" value="recommends">
    <input type="hidden" name="audioId" value="">
    <div class="audio-block">
        <input type="text" name="audioFind" value="${audioFind}">
        <input type="submit" value="Поиск" class="audio-button">
    </div>
    <c:set var="audioList" value="${audiotracks}"/>
    <c:choose>
        <c:when test="${not empty audioList}">
            <c:forEach var="elem" items="${audiotracks}" begin="${(page-1)*6}" end="${page*6-1}">
                <div class="audio-block" id="${elem.id}">
                    <div class="audio-name">
                        <span> ${elem.author} - ${elem.name}</span>
                    </div>
                    <div class="info-img">
                        <a href="">
                            <img class="info-img" src="resource/images/information-icon.png">
                        </a>
                    </div>
                    <div class="audio-price">
                        <span> ${elem.price} $</span>
                    </div>
                    <input type="submit" name="addToBasket" value="Добавить в корзину" class="audio-button">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="audio-block">
                <span>Не найдено ни одного рекоммендуемого аудиотрека</span>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="audio-block">
        <c:set var="pageSize" value="${pageSize}"/>
        <c:if test="${pageSize > 1}">
            <c:forEach begin="1" end="${pageSize}" varStatus="step">
                <input type="submit" name="pageButton" value="${step.count}" class="page-button"/>
            </c:forEach>
        </c:if>
    </div>
</form>
<footer class="footer">(c) E. Alekhnovich & G. Ivko</footer>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/recommends.js"></script>
</body>
</html>
