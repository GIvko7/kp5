<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/mainAdmin.css">
    <title>Главная</title>
<body>
<header class="header">
    Главная
    <a href="/course_project/audio_store?commandObject=audioTrack&command=find">Аудиотреки</a>
    <a onclick="exit()" href="/course_project/audio_store">Выход</a>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="client">
    <input type="hidden" name="command" value="find">
    <input type="hidden" name="clientId" value="">
    <input type="hidden" name="bonusId" value="">
    <div class="client-block">
        <input type="text" name="clientFind" value="${clientFind}">
        <input type="submit" value="Поиск" class="audio-button">
        <input type="submit" id="addAudio" value="Добавить новый бонус" class="add-button">
    </div>
    <c:set var="clientList" value="${clients}"/>
    <c:choose>
        <c:when test="${not empty clientList}">
            <c:forEach var="elem" items="${clients}" begin="${(page-1)*4}" end="${page*4-1}">
                <div class="client-block" id="${elem.id}">
                    <div class="client">
                        <span> ${elem.name} (${elem.login})</span>
                        <c:set var="bonus" value="${elem.bonus}"/>
                        <c:choose>
                            <c:when test="${not empty bonus}">
                                <p id="bonus">Бонус: ${elem.bonus.name} ${elem.bonus.size} ${elem.bonus.units}</p>
                            </c:when>
                            <c:otherwise>
                                <p id="bonus">Бонус отсутствует</p>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <input type="button" name="addBonus" value="Изменить бонус" class="audio-button">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="client-block">
            <div class="client">
                <span>Не найдено ни одного клиента</span>
            </div>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="client-block">
        <c:set var="pageSize" value="${pageSize}"/>
        <c:if test="${pageSize > 1}">
            <c:forEach begin="1" end="${pageSize}" varStatus="step">
                <input type="submit" name="pageButton" value="${step.count}" class="page-button"/>
            </c:forEach>
        </c:if>
    </div>
    <div class="modal-form" id="modalClient">
        <span class="modal_close">X</span>
        <div class="form-item">
            <select id="select">
                <c:forEach var="elem" items="${listBonus}">
                    <option value=${elem.id}>${elem.name} ${elem.size} ${elem.units}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-item">
            <input type='submit' value='Сохранить' class="audio-button">
            <input type='reset' value='Очистить' class="audio-button">
        </div>
    </div>
    <div class="modal-form" id="modalAdd">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="name" class="form-item-label">Название</label>
            <input id="name" type="hidden" class="form-input" name="name">
            <select id="selectName">
                <option>Бесплатный аудиотрек</option>
                <option>Скидка</option>
            </select>
        </div>
        <div class="form-item">
            <label for="size" class="form-item-label">Количество</label>
            <input id="size" class="form-input" name="size">
            <span name="error-size" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="units" class="form-item-label">Единицы измерения</label>
            <input id="units" class="form-input" name="units">
            <span name="error-units" class="error-message"></span>
        </div>
        <div class="form-item">
            <input type='submit' value='Сохранить' class="audio-button">
            <input type='reset' value='Очистить' class="audio-button">
        </div>
    </div>
    <div id="overlay"></div>
</form>
<footer class="footer">(c) E. Alekhnovich & G. Ivko</footer>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/mainAdmin.js"></script>
<script src="js/validationBonus.js"></script>
</body>
</html>
