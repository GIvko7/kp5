<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 16.04.2017
  Time: 0:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/audiotracks.css">
    <title>Корзина</title>
</head>
<body>
<header class="header" method="GET">
    <a href="/course_project/audio_store" >Главная</a>
    <a href="/course_project/audio_store?commandObject=audioTrack&command=find">Аудиотреки</a>
    Корзина
    <a href="/course_project/audio_store?commandObject=audioTrack&command=recommends">Рекомендации</a>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="order">
    <input type="hidden" name="command" value="find">
    <input type="hidden" name="audioId" value="">
    <c:set var="audioList" value="${audiotracks}"/>
    <c:choose>
        <c:when test="${not empty audioList}">
            <c:forEach var="elem" items="${audiotracks}">
                <div class="audio-block" id="${elem.id}">
                    <div class="audio-name">
                        <span> ${elem.author} - ${elem.name}</span>
                    </div>
                    <div class="info-img">
                        <a href="">
                            <img class="info-img" src="resource/images/information-icon.png">
                        </a>
                    </div>
                    <div class="audio-price">
                        <span name="price"> ${elem.price} $</span>
                    </div>
                    <input type="submit" value="Удалить" class="audio-button">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="audio-block">
                <span>Пока не добавлено ни одного аудиотрека в корзину</span>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="bonus-block">
        <input type="checkbox" id="bonusCheck" value="Бонус" name="${client.bonus.name}" size="${client.bonus.size}"/>
        <label for="bonusCheck" >Использовать бонус</label>
    </div>
    <div class="total-cost">
        <span>Итоговая стоимость:</span>
        <span name="totalCost">0.0 $</span>
        <input type="submit" name="payButton" value="Оплатить" class="audio-button"/>
    </div>
</form>
<footer class="footer">(c) E. Alekhnovich & G. Ivko</footer>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/basket.js"></script>
</body>
</html>
