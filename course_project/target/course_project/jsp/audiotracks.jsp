<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 10.04.2017
  Time: 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/audiotracks.css">
    <title>Аудиотреки</title>
</head>
<body>
<header class="header">
    <a href="/course_project/audio_store">Главная</a>
    Аудиотреки
    <a href="/course_project/audio_store?commandObject=order&command=find">Корзина</a>
    <a href="/course_project/audio_store?commandObject=audioTrack&command=recommends">Рекомендации</a>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="audioTrack">
    <input type="hidden" name="command" value="find">
    <input type="hidden" name="audioId" value="">
    <div class="audio-block">
        <input type="text" name="audioFind" value="${audioFind}">
        <input type="submit" value="Поиск" class="audio-button">
        <input type="submit" id="addAudio" value="Добавить новый аудио трек" class="add-button">
    </div>
    <c:set var="audioList" value="${audiotracks}"/>
    <c:choose>
        <c:when test="${not empty audioList}">
            <c:forEach var="elem" items="${audiotracks}" begin="${(page-1)*6}" end="${page*6-1}">
                <div class="audio-block" id="${elem.id}">
                    <div class="audio-name">
                        <span> ${elem.author} - ${elem.name}</span>
                    </div>
                    <div class="info-img">
                        <a href="">
                            <img class="info-img" src="resource/images/information-icon.png">
                        </a>
                    </div>
                    <div class="audio-price">
                        <span> ${elem.price} $</span>
                    </div>
                    <input type="submit" name="addToBasket" value="Добавить в корзину" class="audio-button">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="audio-block">
                <span> Не найдено ни одного аудиотрека</span>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="audio-block">
        <c:set var="pageSize" value="${pageSize}"/>
        <c:if test="${pageSize > 1}">
            <c:forEach begin="1" end="${pageSize}" varStatus="step">
                <input type="submit" name="pageButton" value="${step.count}" class="page-button"/>
            </c:forEach>
        </c:if>
    </div>
    <div class="modal-form" id="modalAudio">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="author" class="form-item-label">Автор</label>
            <input id="author" class="form-input" name="author">
            <span name="error-author" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="name" class="form-item-label">Название</label>
            <input id="name" class="form-input" name="name">
            <span name="error-name" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="year" class="form-item-label">Год</label>
            <input id="year" class="form-input" name="year">
            <span name="error-year" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="genre" class="form-item-label">Жанр</label>
            <input id="genre" class="form-input" name="genre">
            <span name="error-genre" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="price" class="form-item-label">Цена</label>
            <input id="price" class="form-input" name="price">
            <span name="error-price" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="site" class="form-item-label">Ссылка</label>
            <input id="site" class="form-input" name="site">
            <span name="error-site" class="error-message"></span>
        </div>
        <div class="form-item">
            <input type='submit' value='Сохранить' class="audio-button">
            <input type='reset' value='Очистить' class="audio-button">
        </div>
    </div>
    <div id="overlay"></div>
</form>
<footer class="footer">(c) E. Alekhnovich & G. Ivko</footer>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/audiotracks.js"></script>
<script src="js/validationAudio.js"></script>
</body>
</html>
