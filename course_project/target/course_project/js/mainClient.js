/**
 * Created by Босс on 03.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]');

$('.button-group').on('click', function (event) {
    switch (event.target.value) {
        case 'Редактировать данные':
            $('#name').val($('#spanName').html());
            $('#login').val($('#spanLogin').html());
            $('#date').val($('#spanDate').html());
            $('#creditCard').val($('#spanCredit').html());
            $('#email').val($('#spanEmail').html());
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalClient')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Изменить пароль':
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalPassword')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Выход':
            var cookie_date = new Date();
            cookie_date.setTime(cookie_date.getTime() - 1);
            document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
            document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
            break;

    }
});

$('.info-img').on('click', function (event) {
    var hrefAudio = event.target.parentElement,
        id = hrefAudio.parentElement.getAttribute("id");
    hrefAudio.href = '/course_project/audio_store?commandObject=audioTrack&command=get&audioId='+id;
});

$('#modalPassword').on('click', function (event) {
    switch (event.target.value) {
        case 'Сохранить':
            if (validatePassword()) {
                command.val("updatePassword");
                commandObj.val("client");
                return true;
            }
            return false;
    }
});

$('#modalClient').on('click', function (event) {
    switch (event.target.value) {
        case 'Сохранить':
            if (validateClient()) {
                command.val("update");
                commandObj.val("client");
                return true;
            }
            return false;
    }
});

$('.modal_close, #overlay').click(function () {
    $('.modal-form')
        .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
});
