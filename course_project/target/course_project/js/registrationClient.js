var commandObj = $('[name = commandObject]'),
    command = $('[name = command]');

$('.form').on('click', function (event) {
    switch (event.target.value) {
        case 'Регистрация':
            if (validateForm()) {
                commandObj.val('client');
                command.val('add');
                alert('Регистрация прошла успешно!')
                return true;
            }
            return false;
        case 'Вход':
            commandObj.val('client');
            command.val('authorization');
            return true;
        default:
            break;
    }
    return true;
});

function validateForm(){
    var login = document.getElementById('login'),
        name = document.getElementById('name'),
        date = document.getElementById('date'),
        email = document.getElementById('email'),
        password = document.getElementById('password'),
        passwordRep = document.getElementById('passwordRepeat'),
        creditCard = document.getElementById('creditCard'),
        check = true;
    if(!checkName(name)){
        check = false;
    }
    if(!checkLogin(login)){
        check = false;
    }
    if(!checkDate(date)){
        check = false;
    }
    if(!checkEmail(email)){
        check = false;
    }
    if(!checkPassword(password)){
        check = false;
    }
    if(!checkPasswordRep(password, passwordRep)){
        check = false;
    }
    if(!checkCreditCard(creditCard)){
        check = false;
    }
    return check;
}

