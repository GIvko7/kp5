
/**
 * Created by Босс on 10.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]');

switch(getCookie('clientType')){
    case 'ADMIN':
        $('[name = updateAudio]').css({ display: "inline-block" });
        $('.review').css({ display: "none" });
        $('.header :last-child').remove();
        $('.header :last-child').remove();
        $('.header').append("<a onclick='exit()' href='/course_project/audio_store'>Выход</a>");
        break;
}

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Оставить отзыв':
            if(validateContent()) {
                var id = event.target.parentElement.getAttribute("id");
                command.val("add");
                commandObj.val("review");
                return true;
            }
            return false;
        case 'Редактировать аудиотрек':
            $('#name').val($('#spanName').html());
            $('#author').val($('#spanAuthor').html());
            $('#genre').val($('#spanGenre').html());
            $('#year').val($('#spanYear').html());
            $('#price').val($('#spanPrice').html());
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalAudio')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
    }
});

$('.header a').on('click', function (event) {
    switch (event.target.text()) {
        case 'Выход':
            var cookie_date = new Date();
            cookie_date.setTime(cookie_date.getTime() - 1);
            document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
            document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
            break;
    }
});

$('#modalAudio').on('click', function (event) {
    switch (event.target.value) {
        case 'Сохранить':
            if (validateAudio()) {
                command.val("update");
                commandObj.val("audiotrack");
                return true;
            }
            return false;
    }
});

$('.modal_close, #overlay').click(function () {
    $('.modal-form')
        .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function validateContent(){
    var content = $('[name = reviewText]'),
        error = $('[name = error-review]'),
        regExp = /^[-\w\s?,;.:!()а-яА-я]{10,}$/i,
        contentValue = content.val();
    error.css('display', 'none');
    content.css('boxShadow', 'none');
    if (!contentValue) {
        content.css('boxShadow', '0px 0px 5px 5px rgba(255, 178, 178, 1)');
        return false;
    }
    if(contentValue.length < 10){
        error.css('display', 'inline-block');
        error.html('Длина не менее 10 символов.');
        return false;
    }
    if(!regExp.test(contentValue)){
        error.css('display', 'inline-block');
        error.html('Допустимые символы: А-я, A-z, знаки препинания.');
        return false;
    }
    return true;
}

function exit() {
    var cookie_date = new Date();
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
    document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
}

