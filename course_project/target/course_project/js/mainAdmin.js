/**
 * Created by Босс on 24.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]'),
    clientId = $('[name = clientId]'),
    bonusId = $('[name = bonusId]');

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Добавить новый бонус':
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalAdd')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Изменить бонус':
            clientId.val(event.target.parentElement.getAttribute("id"));
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalClient')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Поиск':
            command.val("find");
            commandObj.val("client");
            break;

    }
});

function exit() {
    var cookie_date = new Date();
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
    document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
}

$('#modalClient').on('click', function (event) {
    switch (event.target.value) {
        case 'Сохранить':
            command.val("updateBonus");
            commandObj.val("client");
            bonusId.val($("#select option:selected").val());
            return true;

    }
    return false;
});

$('#modalAdd').on('click', function (event) {
    switch (event.target.value) {
        case 'Сохранить':
            if (validateBonus()) {
                var bonusName = $("#selectName option:selected").text();
                $('#name').val(bonusName);
                command.val("add");
                commandObj.val("bonus");
                return true;

            }
            return false;
    }
});

$('.modal_close, #overlay').click(function () {
    $('.modal-form')
        .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
});