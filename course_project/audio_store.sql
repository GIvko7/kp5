CREATE DATABASE  IF NOT EXISTS `audio_store` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `audio_store`;
-- MySQL dump 10.13  Distrib 5.7.15, for Win32 (AMD64)
--
-- Host: localhost    Database: audio_store
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio_track`
--

DROP TABLE IF EXISTS `audio_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audio_track` (
  `audiotrack_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Audio track id',
  `audiotrack_name` varchar(225) NOT NULL COMMENT 'Audio track name',
  `author` varchar(225) NOT NULL COMMENT 'Audio track author',
  `year` smallint(4) DEFAULT NULL COMMENT 'Audio track year',
  `genre` varchar(45) DEFAULT NULL COMMENT 'Audio track genre',
  `price` decimal(10,1) NOT NULL COMMENT 'Audio track price',
  `site` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`audiotrack_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Table audio_track contains all information about audio track';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_track`
--

LOCK TABLES `audio_track` WRITE;
/*!40000 ALTER TABLE `audio_track` DISABLE KEYS */;
INSERT INTO `audio_track` VALUES (1,'Rape Me','Nirvana',1980,'Рок',0.7,'resources/track.mp3'),(2,'Like A Rolling Stone','Боб Дилан',1965,'Рок',1.5,'resources/track.mp3'),(3,'Imagine','Джон Леннон',1971,'Рок',2.0,'resources/track.mp3'),(4,'Blowin in the Wind','Боб Дилан',1963,'Фолк',1.2,'resources/track.mp3'),(5,'Yesterday','The Beatles',1965,'Поп',1.6,'resources/track.mp3'),(6,'No Woman, No Cry','Bob Marley & The Wailers',NULL,'Регги',1.2,'resources/track.mp3'),(7,'Let It Be','The Beatles',1970,'Поп',2.1,'resources/track.mp3'),(8,'Crazy','Gnarls Barkley',2006,'Pop',1.9,'resources/track.mp3'),(9,'Smells Like Teen Spirit','Nirvana',1991,'Гранж',1.7,'resources/track.mp3'),(11,'Lithium','Nirvana',1980,'Рок',1.1,'resources/track.mp3'),(12,'Came','Bob Marley',1978,'Регги',1.1,'resources/track.mp3'),(13,'Сидней','Аффинаж',2016,'Pop',1.3,'resources/track.mp3'),(15,'Лес','Аффинаж',2016,'Поп',0.8,'resources/track.mp3'),(16,'Гора','Земфира',2007,'Рок',1.9,'resources/track.mp3'),(17,'Прогулка','Земфира',2005,'Рок',2.3,'resources/track.mp3'),(18,'Taro','Alt-J',2015,'Инди-рок',2.1,'resources/track.mp3');
/*!40000 ALTER TABLE `audio_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bonus`
--

DROP TABLE IF EXISTS `bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bonus` (
  `bonus_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Bonus id',
  `bonus_name` varchar(225) NOT NULL COMMENT 'Bonus name',
  `size` smallint(3) NOT NULL COMMENT 'Bonus size',
  `units` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`bonus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Table bonus';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bonus`
--

LOCK TABLES `bonus` WRITE;
/*!40000 ALTER TABLE `bonus` DISABLE KEYS */;
INSERT INTO `bonus` VALUES (1,'Скидка',5,'%'),(2,'Скидка',10,'%'),(3,'Скидка',15,'%'),(4,'Бесплатный аудиотрек',1,'шт'),(5,'Бесплатный аудиотрек',2,'шт');
/*!40000 ALTER TABLE `bonus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Client id',
  `client_name` varchar(225) NOT NULL COMMENT 'Client name',
  `date_birth` date DEFAULT NULL COMMENT 'Client date of birth',
  `email` varchar(45) DEFAULT NULL COMMENT 'Client email',
  `credit_card` varchar(20) DEFAULT NULL COMMENT 'Number of clients credit card',
  `login` varchar(45) NOT NULL COMMENT 'Client login',
  `password` varchar(45) NOT NULL COMMENT 'Client password',
  `bonus_id` int(11) DEFAULT NULL COMMENT 'Bonus id from table bonus',
  PRIMARY KEY (`client_id`),
  KEY `FK_client_bonus_idx` (`bonus_id`),
  CONSTRAINT `FK_client_bonus` FOREIGN KEY (`bonus_id`) REFERENCES `bonus` (`bonus_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Client table contains all information about client';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'Алексей','1991-10-09','alex_po@gmail.com','143256781432','alex','81dc9bdb52d04dc20036dbd8313ed055',1),(2,'Анна','1998-09-22','anna_brek@gmail.com','561256781432','anya98','d93591bdf7860e1e4ee2fca799911215',2),(3,'Николай','1994-03-04','nikol-ret11@gmail.com','2349018765437891','starReb','b59c67bf196a4758191e42f76670ceba',3),(4,'Игорь','1994-12-12','luzhaikino@mail.ru','56712340934','nikNik','934b535800b1cba8f96a5d72f72f1611',1),(5,'Филя','1999-01-06','filTokarev@yandex.ru','908712568123','tokarik97','2be9bd7a3434f7038ca27d1918de58bd',4),(6,'Алена','1991-05-09','serioNoke10@gmail.com','456123450101','alena123','dbc4d84bfcfe2284ba11beffb853a8c4',NULL),(7,'Даша','1996-12-20','darya_wrevick@gmail.com','1651243045671876','das_Intro','7ac186f0544e4f44a40e5c8b0ddc6878',4),(8,'Владислав','1995-01-12','vlad-kuz@mail.ru','9323423422','vladik95','eb68ab2481cf05b45bb8c37f56ec1411',NULL),(9,'Елена','1997-03-16','lenaalekhnovich@gmail.com','456812309812','lena97','eb68ab2481cf05b45bb8c37f56ec1411',2),(10,'Владислав','1998-02-01','vlad_gruzin@mail.ru','1234555345134','vlad1998','896e9754215fb82e10395a5f3c1f490d',NULL),(11,'Василиса','1991-02-01','vasya@gmail.com','1234235235','vasya91','7ac186f0544e4f44a40e5c8b0ddc6878',NULL),(12,'Вася','1990-03-12','vasya12@gmail.com','1234567891','vasya90','eb68ab2481cf05b45bb8c37f56ec1411',NULL);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `client_id` int(11) NOT NULL COMMENT 'Client id',
  `audiotrack_id` int(11) NOT NULL COMMENT 'Audio track id',
  `state` varchar(45) NOT NULL COMMENT 'Order state',
  PRIMARY KEY (`client_id`,`audiotrack_id`),
  KEY `FK_order_audiotrack_idx` (`audiotrack_id`),
  KEY `FK_order_client_idx` (`client_id`),
  CONSTRAINT `FK_order_audiotrack` FOREIGN KEY (`audiotrack_id`) REFERENCES `audio_track` (`audiotrack_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_order_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table order contains information about order and describes relation M:M between audio_track and client';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,1,'В корзине'),(1,3,'В корзине'),(1,6,'Оплачен'),(2,5,'Оплачен'),(3,2,'В корзине'),(3,3,'Оплачен'),(3,5,'Оплачен'),(4,1,'Оплачен'),(4,2,'В корзине'),(5,1,'Оплачен'),(5,2,'Оплачен'),(5,4,'Оплачен'),(5,5,'В корзине'),(7,1,'Оплачен'),(7,8,'Оплачен'),(7,12,'В корзине'),(7,15,'Оплачен'),(9,12,'В корзине'),(11,3,'В корзине'),(11,6,'Оплачен'),(11,8,'В корзине'),(11,11,'Оплачен'),(11,12,'Оплачен'),(12,6,'Оплачен'),(12,8,'Оплачен'),(12,11,'Оплачен'),(12,12,'Оплачен'),(12,18,'Оплачен');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `client_id` int(11) NOT NULL COMMENT 'Client id',
  `audiotrack_id` int(11) NOT NULL COMMENT 'Audio track id',
  `content` varchar(500) NOT NULL COMMENT 'Review content',
  `date` date NOT NULL,
  PRIMARY KEY (`client_id`,`audiotrack_id`),
  KEY `FK_review_audiotrack_idx` (`audiotrack_id`),
  KEY `FK_review_client_idx` (`client_id`),
  CONSTRAINT `FK_review_audiotrack` FOREIGN KEY (`audiotrack_id`) REFERENCES `audio_track` (`audiotrack_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_review_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table review contains information about audio track''s reviews and describes relation M:M between client and audio_track';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,1,'Прекрасная мелодия!','2017-09-01'),(1,9,'Шедеврально.','2015-01-01'),(2,1,'Полюбившаяся песня.','2016-10-29'),(2,3,'Всегда поднимает настроение','2015-07-14'),(3,6,'Потрясающий аудиотрек, стал одним из моих юбимых.','2017-01-02'),(4,8,'Если честно, ожидал большего.','2017-08-21'),(7,2,'Очень нравится этот аудиотрек','2017-04-23'),(7,6,'Очень понравился','2017-04-26'),(7,7,'Моя любимая композиция','2017-04-23'),(7,9,'Любимая композиция этой группы','2017-04-25'),(7,12,'Превосходный аудио трек!','2017-05-04'),(7,13,'Воодушевляюще','2017-05-02'),(9,1,'Любимая группа','2017-05-01'),(11,8,'Так себе, не очень понравился','2017-04-28'),(12,11,'Прекрасный аудио трек!','2017-05-04');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'audio_store'
--
/*!50003 DROP PROCEDURE IF EXISTS `findAudioForClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `findAudioForClient`(IN cl_id INT, IN find VARCHAR(45))
BEGIN
	SELECT *
    FROM audio_track
    WHERE audio_track.audiotrack_id NOT IN(
		SELECT audiotrack_id 
        FROM `order` 
        WHERE `order`.client_id = cl_id)
        AND (author LIKE find OR audiotrack_name LIKE find)
	ORDER BY author ; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `findRecommendsForClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `findRecommendsForClient`(IN cl_id INT, IN find VARCHAR(45))
BEGIN
SELECT * 
FROM audio_track
WHERE genre IN
(SELECT audio_track.genre 
FROM `order` 
INNER JOIN audio_track ON `order`.audiotrack_id = audio_track.audiotrack_id
WHERE `order`.client_id = cl_id 
UNION ALL
SELECT audio_track.genre 
FROM review
INNER JOIN audio_track ON review.audiotrack_id = audio_track.audiotrack_id WHERE review.client_id = cl_id)
AND audio_track.audiotrack_id NOT IN(
		SELECT audiotrack_id 
        FROM `order` 
        WHERE `order`.client_id = cl_id)
        AND (author LIKE find OR audiotrack_name LIKE find)
ORDER BY author;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAudiosForClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAudiosForClient`(IN cl_id INT)
BEGIN
	SELECT *
    FROM audio_track
    WHERE audio_track.audiotrack_id NOT IN(
		SELECT audiotrack_id 
        FROM `order` 
        WHERE `order`.client_id = cl_id)
	ORDER BY author; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getClientOrders` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getClientOrders`(IN cl_id INT, IN order_state VARCHAR(45))
BEGIN
	SELECT audio_track.audiotrack_id, audiotrack_name, author, year, genre, price, site
    FROM `order`
    INNER JOIN audio_track ON `order`.audiotrack_id = audio_track.audiotrack_id
    WHERE `order`.client_id = cl_id AND `order`.state = order_state
    ORDER BY author;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-04 16:00:51
