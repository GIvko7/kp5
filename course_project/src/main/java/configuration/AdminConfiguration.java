package configuration;

import java.util.ResourceBundle;

/**
 * Created by Босс on 10.04.2017.
 */
public class AdminConfiguration {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("admin");
    private AdminConfiguration() { }
    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
