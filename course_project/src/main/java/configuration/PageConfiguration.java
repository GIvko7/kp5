package configuration;

import java.util.ResourceBundle;

/**
 * Created by Босс on 06.03.2017.
 */
public class PageConfiguration {

    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
    private PageConfiguration() { }
    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
