package objects;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Босс on 27.03.2017.
 */
@Entity
@Table(name = "bonus")
public class Bonus implements Serializable {

    private int id;
    private String name;
    private short size;
    private String units;

    @Id
    @Column(name = "bonus_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "bonus_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "size")
    public short getSize() {
        return size;
    }

    public void setSize(short size) {
        this.size = size;
    }

    @Basic
    @Column(name = "units")
    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bonus bonus = (Bonus) o;

        if (id != bonus.id) return false;
        if (size != bonus.size) return false;
        if (name != null ? !name.equals(bonus.name) : bonus.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) size;
        return result;
    }
}
