package objects;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Босс on 27.03.2017.
 */
@Entity

@Table(name = "`order`")
public class Order implements Serializable {
    private int clientId;
    private int audiotrackId;
    private String state;
    private Client client;
    private AudioTrack audioTrack;

    @Id
    @Column(name = "client_id")
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Id
    @Column(name = "audiotrack_id")
    public int getAudiotrackId() {
        return audiotrackId;
    }

    public void setAudiotrackId(int audiotrackId) {
        this.audiotrackId = audiotrackId;
    }

    @Basic
    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (state != null ? !state.equals(order.state) : order.state != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return state != null ? state.hashCode() : 0;
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "client_id", nullable = false)
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne
    @JoinColumn(name = "audiotrack_id", referencedColumnName = "audiotrack_id", nullable = false)
    public AudioTrack getAudioTrack() {
        return audioTrack;
    }

    public void setAudioTrack(AudioTrack audioTrack) {
        this.audioTrack = audioTrack;
    }
}
