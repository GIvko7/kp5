package objects;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Босс on 27.03.2017.
 */
@Entity
@Table(name = "review")
public class Review implements Serializable {
    private int clientId;
    private int audiotrackId;
    private String content;
    private Date date;
    private Client client;
    private AudioTrack audioTrack;

    @Id
    @Column(name = "client_id")
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Id
    @Column(name = "audiotrack_id")
    public int getAudiotrackId() {
        return audiotrackId;
    }

    public void setAudiotrackId(int audiotrackId) {
        this.audiotrackId = audiotrackId;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        if (content != null ? !content.equals(review.content) : review.content != null) return false;
        if (date != null ? !date.equals(review.date) : review.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = content != null ? content.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "client_id", nullable = false)
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne
    @JoinColumn(name = "audiotrack_id", referencedColumnName = "audiotrack_id", nullable = false)
    public AudioTrack getAudioTrack() {
        return audioTrack;
    }

    public void setAudioTrack(AudioTrack audioTrack) {
        this.audioTrack = audioTrack;
    }
}
