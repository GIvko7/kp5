package database;


import objects.Order;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 06.03.2017.
 */
public class OrderDAO extends AbstractDAO<Order> {

    private static OrderDAO instance = null;
    private static ReentrantLock lock = new ReentrantLock();

    private OrderDAO() {
    }

    public static OrderDAO getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new OrderDAO();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    @Override
    public List<Order> getAll() {
        return null;
    }

    @Override
    public Order getEntityById(int id) {
        return null;
    }

    public List<Order> getOrdersByClientId(int clientId, String state) {
        Session session = null;
        List<Order> orderList = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Order where clientId =:clientId AND state =:state");
            query.setParameter("clientId", clientId);
            query.setParameter("state", state);
            orderList = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return orderList;
    }

    public boolean create(Order entity){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_insert = "INSERT INTO `order` (audiotrack_id, client_id, state) VALUES (?, ?, ?)";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createNativeQuery(sql_insert);
            query.setParameter(1, entity.getAudiotrackId());
            query.setParameter(2, entity.getClientId());
            query.setParameter(3, entity.getState());
            query.executeUpdate();
            transaction.commit();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }

    public boolean delete(Order entity){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_delete = "DELETE FROM `order` WHERE client_id = ? AND audiotrack_id = ?";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createNativeQuery(sql_delete);
            query.setParameter(1, entity.getClientId());
            query.setParameter(2, entity.getAudiotrackId());
            query.executeUpdate();
            transaction.commit();
            flag = true;
        } catch (HibernateException e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }

    public boolean updateAll(int cl_id){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_update = "UPDATE Order SET state='Оплачен' WHERE clientId = " + cl_id;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery(sql_update);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            transaction.rollback();
        }finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }

}
