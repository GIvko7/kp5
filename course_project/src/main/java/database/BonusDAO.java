package database;

import objects.Bonus;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 06.03.2017.
 */
public class BonusDAO extends AbstractDAO <Bonus> {

    private static BonusDAO instance = null;
    private static ReentrantLock lock = new ReentrantLock();

    private BonusDAO(){ }

    public static BonusDAO getInstance(){
        lock.lock();
        try {
            if (instance == null) {
                instance = new BonusDAO();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    @Override
    public List<Bonus> getAll() {
        List<Bonus> listBonus = new LinkedList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            listBonus = session.createQuery("from Bonus ").list();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return listBonus;
    }

    @Override
    public Bonus getEntityById(int bonusId) {
        Bonus bonus = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Bonus where id = :bonusId");
            query.setParameter("bonusId", bonusId);
            bonus = (Bonus) query.uniqueResult();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return bonus;
    }


    public boolean create(Bonus entity){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_insert = "INSERT INTO `bonus` (bonus_name, size, units) VALUES (?, ?, ?)";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createNativeQuery(sql_insert);
            query.setParameter(1, entity.getName());
            query.setParameter(2, entity.getSize());
            query.setParameter(3, entity.getUnits());
            query.executeUpdate();
            transaction.commit();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка при вставке», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }

}
