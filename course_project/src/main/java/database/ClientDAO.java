package database;

import objects.Client;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 06.03.2017.
 */
public class ClientDAO extends AbstractDAO<Client> {

    private static ClientDAO instance = null;
    private static ReentrantLock lock = new ReentrantLock();

    private ClientDAO() {
    }

    public static ClientDAO getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new ClientDAO();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }


    public List<Client> findAll(String strFind){
        List<Client> list = new LinkedList<>();
        Session session = null;
        strFind = "%" + strFind + "%";
        String selectSql = "from Client WHERE name LIKE '" + strFind +
                "' OR login LIKE '" + strFind + "' ORDER BY name";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            list = session.createQuery(selectSql).list();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }


    @Override
    public List<Client> getAll() {
        List<Client> list = new LinkedList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            list = session.createQuery("from Client ").list();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    @Override
    public Client getEntityById(int clientId) {
        Client client = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Client where id = :clientId");
            query.setParameter("clientId", clientId);
            client = (Client)query.uniqueResult();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'findById'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return client;
    }


    @Transactional
    public Client getClientByLogin(String login){
        Session session = null;
        Client client = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Client where login = :login");
            query.setParameter("login", login);
            client = (Client)query.uniqueResult();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'findById'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return client;
    }


}
