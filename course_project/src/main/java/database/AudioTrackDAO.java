package database;

import objects.AudioTrack;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionImpl;
import org.hibernate.query.Query;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 02.03.2017.
 */
public class AudioTrackDAO extends AbstractDAO <AudioTrack>{

    private static AudioTrackDAO instance = null;
    private static ReentrantLock lock = new ReentrantLock();

    private AudioTrackDAO(){ }

    public static AudioTrackDAO getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new AudioTrackDAO();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    @Override
    public List<AudioTrack> getAll() {
        List<AudioTrack> list = new LinkedList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            list = session.createQuery("from AudioTrack ").list();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    @Override
    public AudioTrack getEntityById(int audioId) {
        AudioTrack audioTrack = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from AudioTrack where id = :audioId");
            query.setParameter("audioId", audioId);
            audioTrack = (AudioTrack) query.uniqueResult();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return audioTrack;
    }

    public List<AudioTrack> findRecommendsForClient(int cl_id, String strFind){
        List<AudioTrack> list = new LinkedList<>();
        SessionImpl session = null;
        CallableStatement callSt = null;
        String SQL_SELECT = "{call findRecommendsForClient (?, ?)}";
        try {
            session = (SessionImpl)HibernateUtil.getSessionFactory().openSession();
            Connection con = session.connection();
            callSt = con.prepareCall(SQL_SELECT);
            callSt.setInt(1, cl_id);
            String find = "%" + strFind + "%";
            callSt.setString(2, find);
            ResultSet resultSet = callSt.executeQuery();
            while (resultSet.next()) {
                AudioTrack audioTrack = new AudioTrack();
                audioTrack.setId(resultSet.getInt("audiotrack_id"));
                audioTrack.setName(resultSet.getString("audiotrack_name"));
                audioTrack.setAuthor(resultSet.getString("author"));
                audioTrack.setYear(resultSet.getShort("year"));
                audioTrack.setGenre(resultSet.getString("genre"));
                audioTrack.setPrice(resultSet.getFloat("price"));
                audioTrack.setSite(resultSet.getString("site"));
                list.add(audioTrack);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<AudioTrack> findAudioForClient(int cl_id, String strFind){
        List<AudioTrack> list = new LinkedList<>();
        SessionImpl session = null;
        CallableStatement callSt = null;
        String SQL_SELECT = "{call findAudioForClient (?, ?)}";
        try {
            session = (SessionImpl)HibernateUtil.getSessionFactory().openSession();
            Connection con = session.connection();
            callSt = con.prepareCall(SQL_SELECT);
            callSt.setInt(1, cl_id);
            String find = "%" + strFind + "%";
            callSt.setString(2, find);
            ResultSet resultSet = callSt.executeQuery();
            while (resultSet.next()) {
                AudioTrack audioTrack = new AudioTrack();
                audioTrack.setId(resultSet.getInt("audiotrack_id"));
                audioTrack.setName(resultSet.getString("audiotrack_name"));
                audioTrack.setAuthor(resultSet.getString("author"));
                audioTrack.setYear(resultSet.getShort("year"));
                audioTrack.setGenre(resultSet.getString("genre"));
                audioTrack.setPrice(resultSet.getFloat("price"));
                audioTrack.setSite(resultSet.getString("site"));
                list.add(audioTrack);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<AudioTrack> findAll(String strFind){
        List<AudioTrack> list = new LinkedList<>();
        Session session = null;
        strFind = "%" + strFind + "%";
        String select = "from AudioTrack WHERE name LIKE '" + strFind +
                "' OR author LIKE '" + strFind + "' ORDER BY author";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            list = session.createQuery(select).list();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public boolean delete(AudioTrack entity){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_delete = "DELETE FROM audio_track WHERE audiotrack_id = ?";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createNativeQuery(sql_delete);
            query.setParameter(1, entity.getId());
            query.executeUpdate();
            transaction.commit();
            flag = true;
        } catch (HibernateException e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }
}
