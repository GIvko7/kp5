package database;

import objects.Review;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 06.03.2017.
 */
public class ReviewDAO extends AbstractDAO<Review> {

    private static ReviewDAO instance = null;
    private static ReentrantLock lock = new ReentrantLock();

    private ReviewDAO(){ }

    public static ReviewDAO getInstance(){
        lock.lock();
        try {
            if (instance == null) {
                instance = new ReviewDAO();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    @Override
    public List<Review> getAll() {
        return null;
    }

    @Override
    public Review getEntityById(int id) {
        return null;
    }

    public Review getEntityById(int clientId, int audioId) {
        Review review = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Review where clientId = :clientId and audiotrackId = :audioId");
            query.setParameter("clientId", clientId);
            query.setParameter("audioId", audioId);
            review = (Review) query.uniqueResult();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка 'getAll'», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return review;

    }

    public boolean create(Review entity){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_insert = "INSERT INTO `review` (audiotrack_id, client_id, content, date) VALUES (?, ?, ?,?)";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createNativeQuery(sql_insert);
            query.setParameter(1, entity.getAudiotrackId());
            query.setParameter(2, entity.getClientId());
            query.setParameter(3, entity.getContent());
            query.setParameter(4, new Date(System.currentTimeMillis()));
            query.executeUpdate();
            transaction.commit();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка при вставке», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }

    public boolean update(Review entity){
        boolean flag = false;
        Session session = null;
        Transaction transaction = null;
        String sql_insert = "UPDATE review SET content = ?, date = ?  " +
                "WHERE audiotrack_id = ? AND client_id = ?";
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Query query = session.createNativeQuery(sql_insert);
            query.setParameter(1, entity.getContent());
            query.setParameter(2, entity.getDate());
            query.setParameter(3, entity.getAudiotrackId());
            query.setParameter(4, entity.getClientId());
            query.executeUpdate();
            transaction.commit();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
            //JOptionPane.showMessageDialog(null, e.getMessage(), «Ошибка при вставке», JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return flag;
    }

}
