package servlets;

import command.ActionCommand;
import command.ActionFactory;
import configuration.PageConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Босс on 03.03.2017.
 */
@Controller
public class AudioController {

    @RequestMapping(value = "/audio_store",method = RequestMethod.GET)
    protected ModelAndView doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        return processRequest(request, response);
    }

    @RequestMapping(value = "/audio_store",method = RequestMethod.POST)
    protected ModelAndView doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        return processRequest(request, response);
    }

    private ModelAndView processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView model = new ModelAndView();
        String page = null;
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text / html;charset=UTF-8");
        if(doFilter(request, response) == null){
            page = PageConfiguration.getProperty("path.page.index");
            return new ModelAndView(page);
        }
        request = doFilter(request, response);
        RequestContent requestContent = new RequestContent();
        requestContent.insertAttributes(request, response);
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(requestContent);
        page = command.execute(requestContent);
        if (page != null) {
            request = requestContent.extractValues(request);
            response = requestContent.extractCookies(response);
        } else {
            page = PageConfiguration.getProperty("path.page.error");
        }
        return new ModelAndView(page);
    }

    public HttpServletRequest doFilter(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String command = request.getParameter("command");
        ArrayList<String> possibleCommands = new ArrayList<>();
        possibleCommands.add("login");
        possibleCommands.add("registration");
        possibleCommands.add("add");
        if(command == null){
            command = "";
        }
        String clientType = null;
        String clientId = null;
        Cookie[] cookies = request.getCookies();
        for(int i = 0; i < cookies.length; i++) {
            if(cookies[i].getName().equals("clientId")){
                clientId = cookies[i].getValue();
            }
            if(cookies[i].getName().equals("clientType")) {
                clientType = cookies[i].getValue();
                break;
            }
        }
        if(clientType == null && !possibleCommands.contains(command)){ //если не авторизован
            return null;
        }
        session.setAttribute("clientType", clientType);
        session.setAttribute("clientId", clientId);
        return request;
    }
}