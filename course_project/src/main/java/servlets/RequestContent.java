package servlets;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Босс on 06.03.2017.
 */
public class RequestContent {
    private HashMap<String, Object> requestAttributes;
    private HashMap<String, String> requestParameters;
    private HashMap<String, Object> sessionAttributes;
    private HashMap<String, String> cookies;
    
    public RequestContent(){
        requestAttributes = new HashMap<>();
        requestParameters = new HashMap<>();
        sessionAttributes = new HashMap<>();
        cookies = new HashMap<>();
    }
    
    public HttpServletRequest extractValues(HttpServletRequest request){
        for (Map.Entry entry : requestAttributes.entrySet()) {
            request.setAttribute((String)entry.getKey(), entry.getValue());
        }
        for (Map.Entry entry : sessionAttributes.entrySet()) {
            request.getSession().setAttribute((String)entry.getKey(), entry.getValue());
        }

        return request;
    }

    public HttpServletResponse extractCookies(HttpServletResponse response){
        for (Map.Entry entry : cookies.entrySet()) {
            Cookie cookie = new Cookie((String)entry.getKey(), (String)entry.getValue());
            response.addCookie(cookie);
        }
        return response;
    }
    
    public void insertAttributes(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        String name = null;
        Enumeration<String> paramNames = request.getParameterNames();
        while(paramNames.hasMoreElements()) {
            name = paramNames.nextElement();
            String value = new String(request.getParameter(name).getBytes(request.getCharacterEncoding()),"UTF-8");
            requestParameters.put(name, value);
        }
        Enumeration<String> attrNames = request.getAttributeNames();
        while(attrNames.hasMoreElements()) {
            name = attrNames.nextElement();
            requestAttributes.put(name, request.getAttribute(name));
        }
        Enumeration<String> sessionNames = request.getSession().getAttributeNames();
        while(sessionNames.hasMoreElements()) {
            name = sessionNames.nextElement();
            sessionAttributes.put(name, request.getSession().getAttribute(name));
        }
        Cookie[] cookiesContent = request.getCookies();
        for (int i = 0; i < cookiesContent.length; i++){
            cookies.put(cookiesContent[i].getName(), cookiesContent[i].getValue());
        }
    }

    public String getParameter(String name){
        return requestParameters.get(name);
    }

    public Object getAttribute(String name){
        return requestAttributes.get(name);
    }

    public Object getSessionAttribute(String name){
        return sessionAttributes.get(name);
    }

    public void setAttribute(String name, Object value){
       requestAttributes.put(name, value);
    }

    public void setCookie(String name, String value){
        cookies.put(name,value);
    }
    public void setSessionAttribute(String name, Object value){
        sessionAttributes.put(name, value);
    }

    public String getCookie(String name) {
        return cookies.get(name);
    }
}
