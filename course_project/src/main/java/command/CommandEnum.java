package command;

/**
 * Created by Босс on 06.03.2017.
 */
public enum CommandEnum {
    ADD,
    UPDATE,
    DELETE,
    FIND,
    FINDALL,
    LOGIN,
    UPDATEPASSWORD,
    REGISTRATION,
    RECOMMENDS,
    ADDRECOMMENDS,
    UPDATEBONUS,
    GET
}
