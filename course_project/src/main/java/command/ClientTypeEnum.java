package command;

/**
 * Created by Босс on 08.03.2017.
 */
public enum ClientTypeEnum {
    GUEST, USER, ADMIN
}
