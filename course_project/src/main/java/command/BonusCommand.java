package command;

import configuration.PageConfiguration;
import database.BonusDAO;
import objects.Bonus;
import servlets.RequestContent;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Босс on 06.03.2017.
 */
public class BonusCommand implements ActionCommand {

    private BonusDAO dbConnection;

    public BonusCommand(){
        dbConnection = BonusDAO.getInstance();
    }

    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        try {
            switch (CommandEnum.valueOf(action.toUpperCase())) {
                case ADD:
                    page = add(request);
                    break;

            }
        } catch (Exception e){
            page = PageConfiguration.getProperty("path.page.error");
        }
        return page;
    }

    public String add(RequestContent request) {
        String page = null;
        Bonus bonus = new Bonus();
        bonus.setName(request.getParameter("name"));
        bonus.setSize(Short.parseShort(request.getParameter("size")));
        bonus.setUnits(request.getParameter("units"));
        dbConnection.create(bonus);
        page = new ClientCommand().find(request);
        return page;
    }


    public String findAll(HttpServletRequest request) {
        return null;
    }


    public String update(HttpServletRequest request) {
        return null;
    }


    public String delete(HttpServletRequest request) {
        return null;
    }
}
