package command;

import servlets.RequestContent;

/**
 * Created by Босс on 06.03.2017.
 */
public class ActionFactory {

    public ActionCommand defineCommand(RequestContent request){
        ActionCommand currentCommand = new EmptyCommand();
        String actionObject = request.getParameter("commandObject");
        if(actionObject == null || actionObject.isEmpty()){
            return currentCommand;
        }
        try{
            TypeCommandEnum currentEnum = TypeCommandEnum.valueOf(actionObject.toUpperCase());
            currentCommand = currentEnum.getCurrentTypeCommand();

        } catch (IllegalArgumentException e){
            request.setAttribute("wrongAction", actionObject);
        }
        return currentCommand;
    }
}
