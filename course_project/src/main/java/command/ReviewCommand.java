package command;

import configuration.PageConfiguration;
import database.ReviewDAO;
import objects.Review;
import servlets.RequestContent;

import java.sql.Date;

/**
 * Created by Босс on 06.03.2017.
 */
public class ReviewCommand implements ActionCommand {

    private ReviewDAO dbConnection;

    public ReviewCommand() {
        dbConnection = ReviewDAO.getInstance();
    }

    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        try {
            switch (CommandEnum.valueOf(action.toUpperCase())) {
                case ADD:
                    page = add(request);
                    break;
                case UPDATE:
                    break;
                case DELETE:
                    break;
                case FIND:
                    break;
            }
        } catch (Exception e) {
            page = PageConfiguration.getProperty("path.page.error");
        }
        return page;
    }

    public String add(RequestContent request) {
        String page = null;
        int audioId = Integer.parseInt(request.getParameter("audioId").toString());
        int clientId = Integer.valueOf((String) request.getSessionAttribute("clientId"));
        String content = request.getParameter("reviewText");
        Review review = new Review();
        review.setClientId(clientId);
        review.setAudiotrackId(audioId);
        review.setContent(content);
        review.setDate(new Date(System.currentTimeMillis()));
        if (dbConnection.getEntityById(clientId, audioId) == null) {
            dbConnection.create(review);
        } else {
            dbConnection.update(review);
        }
        page = (new AudioTrackCommand().getAudio(request));
        return page;
    }
}
