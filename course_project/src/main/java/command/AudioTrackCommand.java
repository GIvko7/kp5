package command;

import configuration.PageConfiguration;
import database.AudioTrackDAO;
import objects.AudioTrack;
import objects.Review;
import servlets.RequestContent;

import java.util.List;

/**
 * Created by Босс on 06.03.2017.
 */
public class AudioTrackCommand implements ActionCommand {

    AudioTrackDAO dbConnection;

    AudioTrackCommand(){
        dbConnection = AudioTrackDAO.getInstance();
    }

    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        try {
            switch (CommandEnum.valueOf(action.toUpperCase())) {
                case ADD:
                    page = add(request);
                    break;
                case UPDATE:
                    page = update(request);
                    break;
                case DELETE:
                    page = delete(request);
                    break;
                case FIND:
                    page = find(request);
                    break;
                case GET:
                    page = getAudio(request);
                    break;
                case RECOMMENDS:
                    page = findRecommends(request);
                    break;
            }
        } catch(Exception e){
            page = PageConfiguration.getProperty("path.page.error");
        }
        return page;
    }

    public String add(RequestContent request) {
        String page = null;
        AudioTrack audiotrack = new AudioTrack();
        audiotrack.setName(request.getParameter("name"));
        audiotrack.setAuthor(request.getParameter("author"));
        audiotrack.setYear(Short.parseShort(request.getParameter("year")));
        audiotrack.setGenre(request.getParameter("genre"));
        audiotrack.setPrice(Float.parseFloat(request.getParameter("price")));
        audiotrack.setSite(request.getParameter("site"));
        dbConnection.create(audiotrack);
        page = find(request);
        return page;
    }

    public String update(RequestContent request){
        String page = null;
        int id = Integer.parseInt(request.getParameter("audioId").toString());
        AudioTrack audiotrack = dbConnection.getEntityById(id);
        audiotrack.setName(request.getParameter("name"));
        audiotrack.setAuthor(request.getParameter("author"));
        audiotrack.setYear(Short.parseShort(request.getParameter("year")));
        audiotrack.setGenre(request.getParameter("genre"));
        audiotrack.setPrice(Float.parseFloat(request.getParameter("price")));
        dbConnection.update(audiotrack);
        page = getAudio(request);
        return page;
    }

    public String find(RequestContent request){
        String page = null;
        List<AudioTrack> list = null;
        String clientType = (String)request.getSessionAttribute("clientType");
        String strFind = request.getParameter("audioFind");
        if(strFind == null){
            strFind = "";
        }
        switch (ClientTypeEnum.valueOf(clientType.toUpperCase())) {
            case USER:
                int id = Integer.parseInt((String) request.getSessionAttribute("clientId"));
                list = dbConnection.findAudioForClient(id, strFind);
                break;
            case ADMIN:
                list = dbConnection.findAll(strFind);
                break;
        }
        request.setAttribute("audioFind", strFind);
        int numberPage = request.getParameter("pageButton") == null ? 1 : Integer.parseInt(request.getParameter("pageButton"));
        int pageSize = list.size() % 6 == 0 ? list.size() / 6 : list.size() / 6 + 1;
        request.setAttribute("page", numberPage);
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("audiotracks", list);
        page = PageConfiguration.getProperty("path.page.audioTracks");
        return page;
    }

    public String get(RequestContent request){
        int id = Integer.parseInt(request.getParameter("audioId"));
        request.setSessionAttribute("audioId", id);
        return getAudio(request);
    }

    public String getAudio(RequestContent request){
        String page = null;
        int id = Integer.parseInt(request.getParameter("audioId"));
        AudioTrack audioTrack = dbConnection.getEntityById(id);
        List<Review> list = audioTrack.getReviewList();
        int numberPage = request.getParameter("pageButton") == null ? 1 : Integer.parseInt(request.getParameter("pageButton"));
        int pageSize = list.size() % 3 == 0 ? list.size() / 3 : list.size() / 3 + 1;
        request.setAttribute("page", numberPage);
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("audiotrack", audioTrack);
        page = PageConfiguration.getProperty("path.page.audioInfo");
        return page;
    }

    public String findRecommends(RequestContent request){
        String page = null;
        List<AudioTrack> list = null;
        String clientType = (String)request.getSessionAttribute("clientType");
        String strFind = request.getParameter("audioFind");
        if(strFind == null){
            strFind = "";
        }
        switch (ClientTypeEnum.valueOf(clientType.toUpperCase())) {
            case USER:
                int id = Integer.parseInt((String) request.getSessionAttribute("clientId"));
                list = dbConnection.findRecommendsForClient(id, strFind);
                break;
            default:
                break;
        }
        request.setAttribute("audioFind", strFind);
        int numberPage = request.getParameter("pageButton") == null ? 1 : Integer.parseInt(request.getParameter("pageButton"));
        int pageSize = list.size() % 6 == 0 ? list.size() / 6 : list.size() / 6 + 1;
        request.setAttribute("page", numberPage);
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("audiotracks", list);
        page = PageConfiguration.getProperty("path.page.recommends");
        return page;
    }

    public String delete(RequestContent request) {
        String page = null;
        int id = Integer.parseInt(request.getParameter("audioId"));
        AudioTrack audioTrack = new AudioTrack();
        audioTrack.setId(id);
        dbConnection.delete(audioTrack);
        page = find(request);
        return page;
    }


}
