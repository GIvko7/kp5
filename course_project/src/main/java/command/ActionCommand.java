package command;

import servlets.RequestContent;

/**
 * Created by Босс on 06.03.2017.
 */
public interface ActionCommand {

    public String execute(RequestContent request);

}
