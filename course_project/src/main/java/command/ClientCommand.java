package command;

import configuration.AdminConfiguration;
import configuration.PageConfiguration;
import database.BonusDAO;
import database.ClientDAO;
import objects.Bonus;
import objects.Client;
import org.apache.commons.codec.digest.DigestUtils;
import servlets.RequestContent;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.List;

/**
 * Created by Босс on 06.03.2017.
 */
public class ClientCommand implements ActionCommand {

    private ClientDAO dbConnection;
    private OrderCommand orderCommand = null;

    public ClientCommand() {
        dbConnection = ClientDAO.getInstance();
        orderCommand = new OrderCommand();
    }

    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        try {
            switch (CommandEnum.valueOf(action.toUpperCase())) {
                case LOGIN:
                    page = logIn(request);
                    break;
                case ADD:
                    page = add(request);
                    break;
                case UPDATE:
                    page = update(request);
                    break;
                case UPDATEPASSWORD:
                    page = updatePassword(request);
                    break;
                case UPDATEBONUS:
                    page = updateBonus(request);
                    break;
                case FIND:
                    page = find(request);
                    break;
                case REGISTRATION:
                    page = registrate();
                default:
                    break;
            }
        } catch(Exception e){
            page = PageConfiguration.getProperty("path.page.error");
        }
        return page;
    }

    public String registrate(){
        return PageConfiguration.getProperty("path.page.registration");
    }

    public String add(RequestContent request) {
        String page = null;
        Client client = new Client();
        client.setName(request.getParameter("name"));
        client.setLogin(request.getParameter("login"));
        client.setDateBirth(Date.valueOf(request.getParameter("date")));
        client.setEmail(request.getParameter("email"));
        client.setCreditCard(request.getParameter("creditCard"));
        client.setPassword(DigestUtils.md5Hex(request.getParameter("password")));
        if(dbConnection.create(client)){
            page = PageConfiguration.getProperty("path.page.index");
        }
        return page;
    }


    public String findAll(HttpServletRequest request) {
        return null;
    }

    public String find(RequestContent request) {
        String page = null;
        List<Client> listClient = null;
        String strFind = request.getParameter("clientFind");
        if(strFind == null){
            strFind = "";
        }
        listClient = dbConnection.findAll(strFind);
        List<Bonus> listBonus= BonusDAO.getInstance().getAll();
        request.setAttribute("listBonus", listBonus);
        request.setAttribute("clientFind", strFind);
        int numberPage = request.getParameter("pageButton") == null ? 1 : Integer.parseInt(request.getParameter("pageButton"));
        int pageSize = listClient.size() % 4 == 0 ? listClient.size() / 4 : listClient.size() / 4 + 1;
        request.setAttribute("page", numberPage);
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("clients", listClient);
        page = PageConfiguration.getProperty("path.page.mainAdmin");
        return page;

    }

    public String updatePassword(RequestContent request) {
        String page = null;
        int clientId = Integer.valueOf((String) request.getSessionAttribute("clientId"));
        Client client = dbConnection.getEntityById(clientId);
        client.setPassword(DigestUtils.md5Hex(request.getParameter("password")));
        dbConnection.update(client);
        client.setOrderList(orderCommand.getClientsAudio(request));
        request.setSessionAttribute("client", client);
        page = PageConfiguration.getProperty("path.page.mainUser");
        return page;

    }

    public String updateBonus(RequestContent request){
        String page = null;
        int clientId = Integer.valueOf((String) request.getParameter("clientId"));
        int bonusId = Integer.parseInt(request.getParameter("bonusId"));
        Client client = dbConnection.getEntityById(clientId);
        Bonus bonus = BonusDAO.getInstance().getEntityById(bonusId);
        client.setBonus(bonus);
        dbConnection.update(client);
        page = find(request);
        return page;
    }

    public String update(RequestContent request) {
        String page = null;
        int clientId = Integer.valueOf((String) request.getSessionAttribute("clientId"));
        Client client = dbConnection.getEntityById(clientId);
        client.setName(request.getParameter("name"));
        client.setLogin(request.getParameter("login"));
        client.setDateBirth(Date.valueOf(request.getParameter("dateBirth")));
        client.setEmail(request.getParameter("email"));
        client.setCreditCard(request.getParameter("creditCard"));
        dbConnection.update(client);
        client.setOrderList(orderCommand.getClientsAudio(request));
        request.setSessionAttribute("client", client);
        page = PageConfiguration.getProperty("path.page.mainUser");
        return page;
    }

    public String delete(HttpServletRequest request) {
        return null;
    }

    public Client findByLogin(RequestContent request) {
        String login = request.getParameter("login");
        Client client = dbConnection.getClientByLogin(login);
        return client;
    }


    public String logIn(RequestContent request) {
        String page = null;
        String clientType = request.getParameter("role");
        ClientTypeEnum clientEnum = clientType == null ? ClientTypeEnum.USER : ClientTypeEnum.ADMIN;
        switch (clientEnum) {
            case USER:
                page = userLogIn(request);
                break;
            case ADMIN:
                page = adminLogIn(request);
                break;
        }
        return page;
    }

    public String adminLogIn(RequestContent request) {
        String page = null;
        String login = request.getParameter("login");
        String password = DigestUtils.md5Hex(request.getParameter("password"));
        String trueLogin = AdminConfiguration.getProperty("admin.login");
        String truePassword = AdminConfiguration.getProperty("admin.password");
        if (!(login.equals(trueLogin) && password.equals(truePassword))) {
            request.setAttribute("error", "Неверный логин или пароль");
            request.setAttribute("login", login);
            page = PageConfiguration.getProperty("path.page.index");
            return page;
        }
        request.setSessionAttribute("clientType", "ADMIN");
        request.setCookie("clientType", "ADMIN");
        page = find(request);
        return page;
    }

    public String userLogIn(RequestContent request) {
        String page = null;
        String login = request.getParameter("login");
        String password = DigestUtils.md5Hex(request.getParameter("password"));
        Client client = findByLogin(request);
        if (client == null) {
            request.setAttribute("error", "Неверный логин или пароль");
            request.setAttribute("login", login);
            page = PageConfiguration.getProperty("path.page.index");
            return page;
        }
        String truePassword = client.getPassword();
        if (password.equals(truePassword)) {
            request.setSessionAttribute("clientId", ((Integer) client.getId()).toString());
            request.setSessionAttribute("clientType", "USER");
            client.setOrderList(orderCommand.getClientsAudio(request));
            request.setSessionAttribute("client", client);
            request.setCookie("clientId", ((Integer) client.getId()).toString());
            request.setCookie("clientType", "USER");
            page = PageConfiguration.getProperty("path.page.mainUser");
        } else {
            request.setAttribute("error", "Неверный логин или пароль");
            request.setAttribute("login", login);
            page = PageConfiguration.getProperty("path.page.index");
        }
        return page;
    }
}
