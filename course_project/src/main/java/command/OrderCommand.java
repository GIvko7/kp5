package command;

import configuration.PageConfiguration;
import database.ClientDAO;
import database.OrderDAO;
import objects.AudioTrack;
import objects.Client;
import objects.Order;
import servlets.RequestContent;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 06.03.2017.
 */
public class OrderCommand implements ActionCommand {

    private OrderDAO dbConnection;

    public OrderCommand() {
        dbConnection = OrderDAO.getInstance();
    }

    @Override
    public String execute(RequestContent request) {
        String page = null;
        String action = request.getParameter("command");
        try {
            switch (CommandEnum.valueOf(action.toUpperCase())) {
                case ADD:
                    page = add(request);
                    break;
                case DELETE:
                    page = delete(request);
                    break;
                case FIND:
                    page = find(request);
                    break;
                case ADDRECOMMENDS:
                    page = addFromRecommends(request);
                    break;
                case UPDATE:
                    page = update(request);
                    break;
                case UPDATEBONUS:
                    page = updateWithBonus(request);
                    break;
            }
        } catch (Exception e) {
            page = PageConfiguration.getProperty("path.page.error");
        }
        return page;
    }

    public String updateWithBonus(RequestContent request) {
        String page = null;
        page = update(request);
        Client client = (Client) request.getSessionAttribute("client");
        client.setBonus(null);
        ClientDAO.getInstance().update(client);
        return page;
    }

    public String find(RequestContent request) {
        String page = null;
        List<AudioTrack> listAudio = new LinkedList<>();
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        List<Order> listOrders = dbConnection.getOrdersByClientId(clientId, "В корзине");
        for (Order listOrder : listOrders) {
            listAudio.add(listOrder.getAudioTrack());
        }
        request.setAttribute("audiotracks", listAudio);
        Client client = ClientDAO.getInstance().getEntityById(clientId);
        request.setSessionAttribute("client", client);
        page = PageConfiguration.getProperty("path.page.basket");
        return page;
    }


    public String add(RequestContent request) {
        String page = null;
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        int audioId = Integer.parseInt((String) request.getParameter("audioId"));
        Order order = new Order();
        order.setClientId(clientId);
        order.setAudiotrackId(audioId);
        order.setState("В корзине");
        dbConnection.create(order);
        page = new AudioTrackCommand().find(request);
        return page;
    }

    public String addFromRecommends(RequestContent request) {
        String page = add(request) == null ? null : PageConfiguration.getProperty("path.page.recommends");
        return page;
    }


    public String findAll(HttpServletRequest request) {
        return null;
    }


    public String update(RequestContent request) {
        String page = null;
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        dbConnection.updateAll(clientId);
        Client client = ClientDAO.getInstance().getEntityById(clientId);
        client.setOrderList(new OrderCommand().getClientsAudio(request));
        request.setSessionAttribute("client", client);
        page = PageConfiguration.getProperty("path.page.mainUser");
        return page;
    }


    public String delete(RequestContent request) {
        String page = null;
        int audioId = Integer.parseInt(request.getParameter("audioId"));
        int clientId = Integer.parseInt((String) request.getSessionAttribute("clientId"));
        Order order = new Order();
        order.setClientId(clientId);
        order.setAudiotrackId(audioId);
        dbConnection.delete(order);
        page = find(request);
        return page;

    }

    public List<Order> getClientsAudio(RequestContent request) {
        List<Order> listOrder = new LinkedList<>();
        int clientId = Integer.parseInt(request.getSessionAttribute("clientId").toString());
        listOrder = dbConnection.getOrdersByClientId(clientId, "Оплачен");
        return listOrder;
    }


}
