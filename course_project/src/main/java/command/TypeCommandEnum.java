package command;

/**
 * Created by Босс on 06.03.2017.
 */
public enum TypeCommandEnum {
    AUDIOTRACK {
        {
            this.typeCommand = new AudioTrackCommand();
        }
    },
    CLIENT{
        {
            this.typeCommand = new ClientCommand();
        }
    },
    BONUS{
        {
            this.typeCommand = new BonusCommand();
        }
    },
    ORDER{
        {
            this.typeCommand = new OrderCommand();
        }
    },
    REVIEW{
        {
            this.typeCommand = new ReviewCommand();
        }
    };

    ActionCommand typeCommand;
    public ActionCommand getCurrentTypeCommand(){
        return typeCommand;
    }

}
