package command;

import configuration.PageConfiguration;
import database.BonusDAO;
import database.ClientDAO;
import objects.Bonus;
import objects.Client;
import servlets.RequestContent;

import java.util.List;

/**
 * Created by Босс on 06.03.2017.
 */
public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(RequestContent request) {
        String page = null;
        String clientType = (String)request.getSessionAttribute("clientType");
        try {
            switch (ClientTypeEnum.valueOf(clientType.toUpperCase())) {
                case USER:
                    int id = Integer.parseInt((String) request.getSessionAttribute("clientId"));
                    Client client = ClientDAO.getInstance().getEntityById(id);
                    client.setOrderList(new OrderCommand().getClientsAudio(request));
                    request.setSessionAttribute("client", client);
                    page = PageConfiguration.getProperty("path.page.mainUser");
                    break;
                case ADMIN:
                    String strFind = "";
                    List<Client> listClient = ClientDAO.getInstance().findAll(strFind);
                    List<Bonus> listBonus = BonusDAO.getInstance().getAll();
                    request.setAttribute("listBonus", listBonus);
                    request.setAttribute("clientFind", strFind);
                    int numberPage = request.getParameter("pageButton") == null ? 1 : Integer.parseInt(request.getParameter("pageButton"));
                    int pageSize = listClient.size() % 4 == 0 ? listClient.size() / 4 : listClient.size() / 4 + 1;
                    request.setAttribute("page", numberPage);
                    request.setAttribute("pageSize", pageSize);
                    request.setAttribute("clients", listClient);
                    page = PageConfiguration.getProperty("path.page.mainAdmin");
                    break;

            }
        } catch(Exception e){
            page = PageConfiguration.getProperty("path.page.error");
        }
       return page;
    }
}
