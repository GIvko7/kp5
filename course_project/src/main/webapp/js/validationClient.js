
/**
 * Created by Босс on 03.04.2017.
 */
function validateClient(){
    var login = document.getElementById('login'),
        name = document.getElementById('name'),
        date = document.getElementById('date'),
        email = document.getElementById('email'),
        creditCard =  document.getElementById('creditCard'),
        check = true;
    if(!checkName(name)){
        check = false;
    }
    if(!checkLogin(login)){
        check = false;
    }
    if(!checkDate(date)){
        check = false;
    }
    if(!checkEmail(email)){
        check = false;
    }
    if(!checkCreditCard(creditCard)){
        check = false;
    }
    return check;
}

function validatePassword(){
    var password = document.getElementById('password'),
        passwordRep = document.getElementById('password-repeat'),
        oldPassword = document.getElementById('oldPassword'),
        trueOldPassword = document.getElementsByName('trueOldPassword')[0],
        check = true;
    if(!checkPassword(password)){
        check = false;
    }
    if(!checkPasswordRep(password, passwordRep)){
        check = false;
    }
    if(!checkTruePassword(oldPassword, trueOldPassword)){
        check = false;
    }
    return check;
}

function checkCreditCard(creditCard){
    var error = document.getElementsByName('error-credit')[0],
        regExp = /^\d{10,16}$/,
        creditValue = creditCard.value;
    error.style.display = 'none';
    if(!toggleInput(creditCard)){
        return false;
    }
    if(!regExp.test(creditValue)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Количество цифр кредитной карты от 10 до 16.';
        return false;
    }
    return true;
}

function checkName(name){
    var error = document.getElementsByName('error-name')[0],
        regExpFirstLetter = /^[А-Я]/,
        regExp = /^[А-Я][а-я]+$/,
        nameValue = name.value;
    error.style.display = 'none';
    if(!toggleInput(name)){
        return false;
    }
    if(!regExpFirstLetter.test(nameValue)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Некорректный первый символ (А-Я)';
        return false;
    }
    if(!regExp.test(nameValue)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Допустимые символы: а-я';
        return false;
    }
    return true;
}

function checkLogin(login){
    var error = document.getElementsByName('error-login')[0],
        regExpFirstLetter = /^[a-zA-Z]/,
        regExp = /^[\w]+$/i,
        loginValue = login.value;
    error.style.display = 'none';
    if(!toggleInput(login)){
        return false;
    }
    if(loginValue.length < 5){
        error.style.display = 'inline-block';
        error.innerHTML = 'Длина не менее 5 символов';
        return false;
    }
    if(!regExpFirstLetter.test(loginValue)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Некорректный первый символ (A-z)';
        return false;
    }
    if(!regExp.test(loginValue)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Допустимые символы: A-z, 0-9, _';
        return false;
    }
    return true;
}

function checkDate(date){
    var error = document.getElementsByName('error-date')[0],
        yearNow = new Date().getFullYear(),
        yearValue = new Date(date.value).getFullYear();
    error.style.display = 'none';
    if(!toggleInput(date)){
        return false;
    }
    if((yearNow - yearValue < 7) || (yearNow - yearValue > 120)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Возраст в дипазоне 7-120 лет';
        return false;
    }
    return true;
}

function checkEmail(email){
    var error = document.getElementsByName('error-email')[0],
        regExp = /^[.\wа-яА-Я-]+@[a-zа-я]{2,5}\.[a-zа-я]{2,3}$/i,
        emailValue = email.value;
    error.style.display = 'none';
    if(!toggleInput(email)){
        return false;
    }
    if(!regExp.test(emailValue)){
        error.style.display = 'inline-block';
        error.innerHTML = 'Некорректный email';
        return false;
    }
    return true;
}

function checkPassword(password){
    var error = document.getElementsByName('error-password')[0],
        pasValue = password.value;
    error.style.display = 'none';
    if(!toggleInput(password)){
        return false;
    }
    if(pasValue.length < 6){
        error.style.display = 'inline-block';
        error.innerHTML = 'Длина не менее 6 символов';
        return false;
    }
    if(!(pasValue.match(/[A-ZА-Я]/) && pasValue.match(/\d/) && pasValue.match(/[a-zа-я]/))){
        error.style.display = 'inline-block';
        error.innerHTML = 'Должны присутствовать цифры, буквы в нижнем и верхнем регистре';
        return false;
    }
    return true;
}

function checkPasswordRep(password, passwordRep){
    var error = document.getElementsByName('error-repeat-password')[0],
        pasValue = password.value,
        pasRepValue = passwordRep.value;
    error.style.display = 'none';
    if(!toggleInput(passwordRep)){
        return false;
    }
    if(pasValue != pasRepValue){
        error.style.display = 'inline-block';
        error.innerHTML = 'Пароли не сопадают';
        return false;
    }
    return true;
}

function checkTruePassword(oldPassword, truePassword){
    var error = document.getElementsByName('error-old-password')[0],
        pasValue = md5(oldPassword.value),
        pasTrueValue = truePassword.value;
    //pasTrueValue = pasTrueValue.substr(0, pasTrueValue.length - 1);
    error.style.display = 'none';
    if(!toggleInput(oldPassword)){
        return false;
    }
    if(pasValue !== pasTrueValue){
        error.style.display = 'inline-block';
        error.innerHTML = 'Введен неверный старый пароль';
        return false;
    }
    return true;
}

function toggleInput(element) {
    if (!element.value) {
        element.style.boxShadow = '0px 0px 5px 5px rgba(255, 178, 178, 1)';
        return false;
    }
    element.style.boxShadow = 'none';
    return true;
}





