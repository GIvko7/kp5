
/**
 * Created by Босс on 10.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]'),
    audioId = $('[name = audioId]');

switch(getCookie('clientType')){
    case 'ADMIN':
        $('[name = addToBasket]').val('Удалить');
        $('#addAudio').css({ display: "inline-block" });
        $('.header :last-child').remove();
        $('.header :last-child').remove();
        $('.header').append("<a onclick='exit()' href='/course_project/audio_store'>Выход</a>");
        break;
}

$('.audio-form').on('click', function (event) {
    switch (event.target.value) {
        case 'Добавить в корзину':
            var id = event.target.parentElement.getAttribute("id");
            command.val("add");
            commandObj.val("order");
            audioId.val(id);
            break;
        case 'Удалить':
            var id = event.target.parentElement.getAttribute("id");
            command.val("delete");
            commandObj.val("audiotrack");
            audioId.val(id);
            break;
        case 'Добавить новый аудио трек':
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modalAudio')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
            break;
        case 'Поиск':
            command.val("find");
            commandObj.val("audiotrack");
            break;

    }
});

$('.info-img').on('click', function (event) {
    var hrefAudio = event.target.parentElement,
        id = hrefAudio.parentElement.parentElement.getAttribute("id");
    hrefAudio.href = '/course_project/audio_store?commandObject=audioTrack&command=get&audioId='+id;
});

function exit() {
    var cookie_date = new Date();
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
    document.cookie = "clientType=; expires=" + cookie_date.toGMTString();
}

$('#modalAudio').on('click', function (event) {
    switch (event.target.value) {
        case 'Сохранить':
            if (validateAudio()) {
                command.val("add");
                commandObj.val("audiotrack");
                return true;
            }
            return false;
    }
});

$('.modal_close, #overlay').click(function () {
    $('.modal-form')
        .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

