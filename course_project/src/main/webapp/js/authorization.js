/**
 * Created by Босс on 12.04.2017.
 */
var commandObj = $('[name = commandObject]'),
    command = $('[name = command]');

var cookie_date = new Date();
cookie_date.setTime(cookie_date.getTime() - 1);
document.cookie = "clientId=; expires=" + cookie_date.toGMTString();
document.cookie = "clientType=; expires=" + cookie_date.toGMTString();

$('.form').on('click', function (event) {
    switch (event.target.value) {
        case 'Регистрация':
            commandObj.val('client');
            command.val('registration');
            break;
        case 'Вход':
            var checkbox = $('[name = role]');
            if(checkbox.prop('checked')){
                checkbox.val('admin')
            }
            break;
        default:
            break;
    }
    return true;
});
