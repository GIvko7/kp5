<%--
  Created by IntelliJ IDEA.
  User: Босс
  Date: 05.03.2017
  Time: 2:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/mainUser.css">
    <title>Главная</title>
</head>
<body>
<header class="header" method="GET">Моя страница
    <a href="/course_project/audio_store?commandObject=audioTrack&command=find">Аудиотреки</a>
    <a href="/course_project/audio_store?commandObject=order&command=find">Корзина</a>
    <a href="/course_project/audio_store?commandObject=audioTrack&command=recommends">Рекомендации</a>
</header>
<form class="mainForm" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="">
    <input type="hidden" name="command" value="">
    <div class="button-group">
        <input type="button" value="Редактировать данные">
        <input type="button" value="Изменить пароль">
        <input type="submit" value="Выход">
    </div>
    <div class="info-about">
        <div class="tittle">
            <p>Информация о себе</p>
        </div>
        <p>Имя</p>
        <span id="spanName">${client.name}</span>
        <p>Логин</p>
        <span id="spanLogin">${client.login}</span>
        <p>Дата рождения</p>
        <span id="spanDate">${client.dateBirth}</span>
        <p>e-mail</p>
        <span id="spanEmail">${client.email}</span>
        <p>Номер кредитной карты</p>
        <span id="spanCredit">${client.creditCard}</span>
        <p>Бонус</p>
        <c:set var="bonus" value="${client.bonus}"/>
        <c:choose>
            <c:when test="${empty bonus}">
                <span>отсутствует</span>
            </c:when>
            <c:otherwise>
                <span>${client.bonus.name} (${client.bonus.size} ${client.bonus.units})</span>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="modal-form" id="modalPassword">
        <span class="modal_close">X</span>
        <div class="form-item">
            <input type="hidden" name="trueOldPassword" value="${client.password}"/>
            <label for="oldPassword" class="form-item-label">Настоящий пароль</label>
            <input type='password' id='oldPassword' class="form-input">
            <span name="error-old-password" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="password" class="form-item-label">Новый пароль</label>
            <input id="password" name="password" class="form-input" type="password">
            <span name="error-password" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="password-repeat" class="form-item-label">Подтвердите пароль</label>
            <input id="password-repeat" class="form-input" type="password">
            <span name="error-repeat-password" class="error-message"></span>
        </div>
        <div class="form-item">
            <input type='submit' value='Сохранить'>
            <input type='reset' value='Очистить'>
        </div>
    </div>
    <div class="modal-form" id="modalClient">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="name" class="form-item-label">Имя</label>
            <input id="name" class="form-input" name="name">
            <span name="error-name" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="login" class="form-item-label">Логин</label>
            <input id="login" class="form-input" name="login">
            <span name="error-login" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="date" class="form-item-label">Дата рождения</label>
            <input type="date" id="date" class="form-input" name="dateBirth">
            <span name="error-date" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="email" class="form-item-label">E-mail</label>
            <input id="email" class="form-input" name="email">
            <span name="error-email" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="creditCard" class="form-item-label">Номер кредитной карты</label>
            <input id="creditCard" class="form-input" name="creditCard">
            <span name="error-credit" class="error-message"></span>
        </div>
        <div class="form-item">
            <input type='submit' value='Сохранить'>
            <input type='reset' value='Очистить'>
        </div>
    </div>
    <div id="overlay"></div>
</form>
<div class="info-audiotrack">
    <div class="tittle">
        <p>Мои аудиотреки</p>
    </div>
    <ul>
        <c:forEach var="elem" items="${client.orderList}">
            <li>${elem.audioTrack.author} - ${elem.audioTrack.name}
                <div class="info-img" id="${elem.audioTrack.id}">
                    <a href="">
                        <img class="info-img" src="resource/images/information-icon.png">
                    </a>
                </div>
                <a href="${elem.audioTrack.site}" download>Скачать</a>
            </li>
        </c:forEach>
    </ul>
</div>
<footer class="footer">(c) E. Alekhnovich & G. Ivko</footer>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/resource/jquery-ui.js"></script>
<script src="js/mainClient.js"></script>
<script src="js/validationClient.js"></script>
<script src="js/MD5.js"></script>
</body>
</html>
