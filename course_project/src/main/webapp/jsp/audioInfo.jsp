<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/audioInfo.css">
    <title>Информация об аудиотреке</title>
</head>
<body>
<header class="header" method="GET">
    <a href="/course_project/audio_store">Главная</a>
    <a href="/course_project/audio_store?commandObject=audioTrack&command=find">Аудиотреки</a>
    <a href="/course_project/audio_store?commandObject=order&command=find">Корзина</a>
    <a href="/course_project/audio_store?commandObject=audioTrack&command=recommends">Рекомендации</a>
</header>
<form class="audio-form" method="POST" action="audio_store">
    <input type="hidden" name="commandObject" value="audiotrack">
    <input type="hidden" name="command" value="get">
    <input type="hidden" name="audioId" value="${audiotrack.id}">
    <div class="tittle">
        <p>Информация об аудиотреке</p>
    </div>
    <div class="audio-info">
        <p>Название</p>
        <span id="spanName">${audiotrack.name}</span>
        <p>Автор</p>
        <span id="spanAuthor">${audiotrack.author}</span>
        <p>Год создания</p>
        <span id="spanYear">${audiotrack.year}</span>
        <p>Жанр</p>
        <span id="spanGenre">${audiotrack.genre}</span>
        <p>Цена</p>
        <span id="spanPrice">${audiotrack.price}</span>
    </div>
    <input type="button" name="updateAudio" value="Редактировать аудиотрек" class="audio-button update-button"/>
    <div class="review">
        <textarea name="reviewText"></textarea>
        <span name="error-review" class="error-message error-content"></span>
        <input type="submit" value="Оставить отзыв" class="audio-button"/>
    </div>
    <div class="tittle">
        <p>Отзывы</p>
    </div>
    <c:set var="reviews" value="${audiotrack.reviewList}"/>
    <c:choose>
        <c:when test="${not empty reviews}">
            <c:forEach var="elem" items="${audiotrack.reviewList}" begin="${(page-1)*3}" end="${page*3-1}">
                <div class="review-info">
                    <div class="reviewer-info">
                        <span>${elem.client.name}, ${elem.date}</span>
                    </div>
                    <div class="review-content">
                        <span>${elem.content}</span>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="review-info">
                <span>Не оставлено ни одного отзыва к данному аудиотреку</span>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="audio-block">
        <c:set var="pageSize" value="${pageSize}"/>
        <c:if test="${pageSize > 1}">
            <c:forEach begin="1" end="${pageSize}" varStatus="step">
                <input type="submit" name="pageButton" value="${step.count}" class="page-button"/>
            </c:forEach>
        </c:if>
    </div>
    <div class="modal-form" id="modalAudio">
        <span class="modal_close">X</span>
        <div class="form-item">
            <label for="author" class="form-item-label">Автор</label>
            <input id="author" class="form-input" name="author">
            <span name="error-author" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="name" class="form-item-label">Название</label>
            <input id="name" class="form-input" name="name">
            <span name="error-name" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="year" class="form-item-label">Год</label>
            <input id="year" class="form-input" name="year">
            <span name="error-year" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="genre" class="form-item-label">Жанр</label>
            <input id="genre" class="form-input" name="genre">
            <span name="error-genre" class="error-message"></span>
        </div>
        <div class="form-item">
            <label for="price" class="form-item-label">Цена</label>
            <input id="price" class="form-input" name="price">
            <span name="error-price" class="error-message"></span>
        </div>
        <div class="form-item">
            <input type='submit' value='Сохранить' class="audio-button">
            <input type='reset' value='Очистить' class="audio-button">
        </div>
    </div>
    <div id="overlay"></div>
</form>
<footer class="footer">(c) E. Alekhnovich & G. Ivko</footer>
<script src="js/resource/jquery-3.1.1.min.js"></script>
<script src="js/audioInfo.js"></script>
<script src="js/validationAudio.js"></script>
</body>
</html>